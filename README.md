# Angular / Electron Assets

Der Pattern Park verwendet zum Darstellen der neueren Aufgaben(mit und ohne UML), sowie zum Anzeigen der Kurzfilme
("Animationen") seit der Version 1.7 eine Electron Anwendung. Langfristig sollen alle Aufgaben und Animationen in die 
Electron App überführt werden.

Das Frontend-Framework Angular ist darauf angewiesen, dass die angezeigten Videos innerhalb des lokalen Http-Servers
erreichbar sind. Da es Electron bis zu diesem Zeitpunkt nicht unterstützt, das Flag `--allow-file-access-from-files` zu
setzen (womit der Zugriff auf eine Datei an eines beliebigen Pfades möglich wäre), müssen die Dateien in Reichweite von 
Angular gebracht werden. Der Inhalt dieses Ordners wird automatisch zu der Electron Anwendung verschoben. Dies erfolgt 
durch einen Automatisierungsprozess (GitLab CI/CD), wenn ein Release-Kandidat erstellt wird.